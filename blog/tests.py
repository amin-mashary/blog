from django.test import TestCase

# Create your tests here.
class SimpleTest(TestCase):
    def test_basic_addition(self):
        self.assertEqual(1 + 2 , 3)
 
    def test_basic_substraction(self):
        self.assertEqual(4 - 2 , 2)